from os import path
from typing import NoReturn, Union
from argparse import ArgumentParser, Namespace

from ftpy import Server, Config

def check_file( parser: ArgumentParser, arg: str ) -> Union[ str, None ]:
    if not path.exists( arg ):
        parser.error( f'The file {arg} does not exist!' )
    else:
        return arg

def main( args: Namespace ) -> NoReturn:
    config = Config( args.config_file )
    server = Server( config )
    server.serve()
    exit( 0 )

if __name__ == '__main__':
    args = ArgumentParser( description='FTPy: a socket file receiver!' )
    args.add_argument(
        '-c',
        dest='config_file',
        type=lambda file: check_file( args, file ),
        metavar='config_file.json',
        help='Server json configuration file',
    )

    main( args.parse_args() )
