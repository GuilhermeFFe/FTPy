Feature: Configuration files
    The configuration abstraction must work with the expected json format, and
    not break with a wrong format. It must also not break when the file does not
    exist - in that case, only the default values must be returned

    Scenario: All values configured
        Given a json file with all fields set
            When getting values from the Config
            Then the values must be returned correctly
    
    Scenario: Extra values configured
        Given a json file with extra fields set
            When getting values from the Config
            Then the values must be returned correctly

    Scenario: Some values configured
        Given a json file with some fields set
            When getting values from the Config
            Then the values must be returned correctly, including the defaults
    
    Scenario: No value configured
        Given no json file
            When getting values from the Config
            Then only the default values must be returned
