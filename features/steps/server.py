from behave import *
from socket import AF_INET, SOCK_STREAM, socket
from os import urandom, remove
from time import sleep
from math import ceil

from ftpy import Server, Config
from ftpy.connection import connection_handler
from ftpy.server import ThreadedTCPServer

cfg = Config()
num_clients = 6
client_index = 0

ConnectionHandler = connection_handler( cfg )

def filename( client_idx: int, file_idx: int ) -> str:
    return f'./features/output/mocked_file_{client_idx}_{file_idx}'

def client( message: bytes ) -> None:
    with socket( AF_INET, SOCK_STREAM ) as sock:
        sock.connect( ( cfg.host, cfg.port ) )
        sock.sendall( message )

def _check_data( idx: int, expected: bytes, size: int ) -> None:
    slice_number = ceil( size/cfg.max_file_size )
    message = b''
    for slice in range( slice_number ):
        with open( filename( idx, slice ), 'rb' ) as file:
            received = file.read()

        recv_sz = len( received )
        message += received
        remove( filename( idx, slice ) )
        if slice == slice_number - 1:
            assert recv_sz <= cfg.max_file_size, 'server stored more than the limit in a single file'
        else:
            assert recv_sz == cfg.max_file_size, 'server stored less data than the limit in a file before the last'

    assert message == expected, 'server stored wrong data'

@given( 'a mocked server' )
def mock_server( context ) -> None:
    class MockedConnectionHandler( ConnectionHandler ):
        def __init__( self, request, client_address, server ):
            global client_index

            self.client_index = client_index
            client_index += 1
            super().__init__( request, client_address, server )

        def make_filename( self ) -> str:
            return filename( self.client_index, self.file_index )

    context.server = Server( cfg )
    context.server.server.server_close()
    context.server.server = ThreadedTCPServer(
        ( cfg.host, cfg.port ), MockedConnectionHandler
    )
    context.server.serve_async()

@when( '{clients} client connect and send {size} bytes of data' )
def connect_clients( context, clients: str, size: str ) -> None:
    global client_index
    client_index = 0

    context.sent = []
    for _ in range( int( clients ) ):
        context.sent.append( urandom( int( size ) ) )
        client( context.sent[ -1 ] )
    sleep( 0.5 )

@then( '{clients} client\'s {size} bytes of data can be stored to the server' )
def check_data( context, clients: str, size: str ) -> None:
    context.server.close()
    for idx in range( int( clients ) ):
        _check_data( idx, context.sent[ idx ], int( size ) )
