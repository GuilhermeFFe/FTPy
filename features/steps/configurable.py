from json import load
from behave import *

from ftpy import Config

@given( 'a json file with {how_many} fields set' )
def all_configurations( context, how_many ):
    filename = f'./features/config/{how_many}.json'

    with open( filename, 'r' ) as f:
        context.original = load( f )

    context.cfg = Config( filename )

@given( 'no json file' )
def no_configuration( context ):
    context.cfg = Config()

@when( 'getting values from the Config' )
def getting_values( context ):
    context.got = {
        'OutputDir': context.cfg.output_dir,
        'Host': context.cfg.host,
        'Port': context.cfg.port,
        'MaxFileSize': context.cfg.max_file_size,
        'Timeout': context.cfg.timeout,
        'FilePrefix': context.cfg.file_prefix
    }

@then( 'the values must be returned correctly' )
def check_all_values( context ):
    for key in context.got:
        assert context.got[key] == context.original[key], \
            f'got different config values than expected.\n' \
            f'  Expected: {key}: {context.original[key]}\n' \
            f'  Received: {key}: {context.got[key]}'

@then( 'the values must be returned correctly, including the defaults' )
def check_some_values( context ):
    for key in context.original:
        assert context.original[key] == context.got[key], \
            f'got different value than expected.\n' \
            f'  Expected: {key}: {context.original[key]}\n' \
            f'  Received: {key}: {context.got[key]}'
    for keyval in [
        ( 'MaxFileSize', 1024 ), ( 'Timeout', 10 ), ( 'OutputDir', '' )
    ]:
        assert context.got[ keyval[ 0 ] ] == keyval[1], \
            f'got different value than expected.\n' \
            f'  Expected: {keyval[0]}: {keyval[1]}\n' \
            f'  Received: {keyval[0]}: {context.got[keyval[0]]}'

@then( 'only the default values must be returned' )
def check_defaults( context ):
    for keyval in [
        ( 'OutputDir', '' ), ( 'Host', 'localhost' ), ( 'Port', 6969 ),
        ( 'MaxFileSize', 1024 ), ( 'Timeout', 10 ), ( 'FilePrefix', '' )
    ]:
        assert context.got[ keyval[ 0 ] ] == keyval[1], \
            f'got different value than expected.\n' \
            f'  Expected: {keyval[0]}: {keyval[1]}\n' \
            f'  Received: {keyval[0]}: {context.got[keyval[0]]}'