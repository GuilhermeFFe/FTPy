Feature: Server listen to a port and saves to file
    The server starts listening on a configuration port and saves received data
    to file correctly.

    Scenario: Server receives data from a client
        Given a mocked server
            When 1 client connect and send 512 bytes of data
            Then 1 client's 512 bytes of data can be stored to the server
    
    Scenario: Server receives data from multiple clients
        Given a mocked server
            When 10 client connect and send 512 bytes of data
            Then 10 client's 512 bytes of data can be stored to the server

    Scenario: Server receives more data than it can store in a single file from a client
        Given a mocked server
            When 1 client connect and send 32768 bytes of data
            Then 1 client's 32768 bytes of data can be stored to the server
    
    Scenario: Server receives more data than it can store in a single file from multiple clients
        Given a mocked server
            When 10 client connect and send 2560 bytes of data
            Then 10 client's 2560 bytes of data can be stored to the server
