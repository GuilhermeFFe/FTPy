FROM python:3.9

COPY . /ftpy/
WORKDIR /ftpy/

RUN pip install -r requirements.txt

CMD ["python", "main.py", "-c", "config.json"]