from datetime import datetime

def timestamp() -> str:
    '''
    Returns a timestamp string in the format:
    '{year}{month}{day}{hour}{minute}{second}{microsecond}'
    '''
    return datetime.now().strftime( '%Y%m%d%H%M%S%f' )