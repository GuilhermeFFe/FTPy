'''
This module contains the configuration abstraction class
'''

from typing import Union
from json import load
from os.path import isdir
from sys import stderr

class Config:
    '''
    A utility class for json reading and dictionary abstraction. This also
    encapsulates default values.
    '''
    def __init__( self, file: str = None ) -> None:
        if file is not None:
            self.file = file
            self.data = self.__load()
            self.validate()
        else:
            self.data = {}

    def validate( self ) -> None:
        if self.output_dir:
            assert isdir(
                self.output_dir
            ), f'output directory "{self.output_dir}" does not exist'

    @property
    def output_dir( self ) -> str:
        return self.data.get( 'OutputDir', '' )

    @property
    def host( self ) -> str:
        return self.data.get( 'Host', 'localhost' )

    @property
    def port( self ) -> int:
        return self.data.get( 'Port', 6969 )

    @property
    def max_file_size( self ) -> int:
        return self.data.get( 'MaxFileSize', 1024 )

    @property
    def timeout( self ) -> int:
        return self.data.get( 'Timeout', 10 )

    @property
    def file_prefix( self ) -> str:
        return self.data.get( 'FilePrefix', '' )

    def __load( self ) -> dict[ str, Union[ str, int ] ]:
        '''
        loads configurations from self.file
        '''

        with open( self.file, 'r' ) as f:
            return load( f )
