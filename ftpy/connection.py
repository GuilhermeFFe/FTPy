'''
A socketserver connection handler. This module exports a function that returns
a subclass of BaseRequestHandler
'''

from io import FileIO
from socketserver import BaseRequestHandler
from socket import timeout

from .utils import timestamp
from .config import Config

def connection_handler( cfg: Config ) -> type[ BaseRequestHandler ]:
    '''
    Creates a BaseRequestHandler with a given cfg object. Returns a subclass of
    BaseRequestHandler
    '''
    class Connection( BaseRequestHandler ):
        '''
        Request handler for the FTPy behavior. This receives data from a TCP
        socket and records it to a sequence of files, following a naming scheme.
        '''
        config = cfg

        def __init__( self, request, client_address, server ):
            print( 'instantiated' )
            self.bytes_read = 0
            self.file_index = -1  # starts at -1 because the first index is going to be zero (check next_file)
            self.file: FileIO = None
            super().__init__( request, client_address, server )

        def recv( self ) -> bytes:
            '''
            Reads one single byte from client
            '''
            return self.request.recv( 1 )

        def close_file( self ) -> None:
            if self.file:
                self.file.flush()
                self.file.close()
                self.file = None

        def next_file( self ) -> None:
            self.file_index += 1
            self.file = open( self.make_filename(), 'wb+' )

        def make_filename( self ) -> str:
            dir = ''
            if self.config.output_dir:
                dir = self.config.output_dir + '/'

            prefix = ''
            if self.config.file_prefix:
                prefix = self.config.file_prefix + '_'

            client = f'{self.client_address[0]}.{self.client_address[1]}'
            return f'{dir}{prefix}{timestamp()}_{client}_{self.file_index}'

        def on_byte( self, byte: bytes ) -> None:
            assert len( byte ) == 1, 'on_byte parameter must be a single byte'

            if self.bytes_read == 0:
                self.next_file()

            self.bytes_read += 1

            self.file.write( byte )

            if self.bytes_read == self.config.max_file_size:
                self.close_file()
                self.bytes_read = 0

        def setup( self ) -> None:
            print( f'setting up connection with {self.client_address}' )
            self.request.settimeout( self.config.timeout )

        def finish( self ) -> None:
            print( f'finishing connection with {self.client_address}' )
            self.close_file()

        def handle( self ) -> None:
            while True:
                try:
                    byte = self.recv()
                    if len( byte ) == 0:
                        # An empty recv means the other side has closed the connection
                        return
                    self.on_byte( byte )
                except timeout:
                    return

    return Connection
