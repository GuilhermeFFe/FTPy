from socketserver import TCPServer, ThreadingMixIn
from threading import Thread

from .connection import connection_handler
from .config import Config

class ThreadedTCPServer( ThreadingMixIn, TCPServer ):
    '''
    This is a help-class to make a TCPServer Threaded, refer to the official
    python socketserver's documentation at:

    https://docs.python.org/3/library/socketserver.html#socketserver.ThreadingMixIn
    '''
    pass

class Server:
    def __init__( self, config: Config ) -> None:
        self.config = config
        self.thread: Thread = None
        self.server = ThreadedTCPServer(
            ( self.config.host, self.config.port ),
            connection_handler( self.config )
        )

    def serve( self ) -> None:
        with self.server:
            print( f'Server listening on port {self.config.port}' )
            self.server.serve_forever()

    def serve_async( self ) -> None:
        print( f'Server listening on port {self.config.port}' )

        self.thread = Thread( target=self.server.serve_forever )

        self.thread.daemon = True
        self.thread.start()

    def join( self ) -> None:
        self.thread.join()

    def close( self ) -> None:
        self.server.shutdown()
        self.join()
