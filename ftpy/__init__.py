'''
A file file transfer server library. This package offers a file storing server
that can receive a configuration object (which is also exported from here).

This server opens a socket on a configured port and receives connections. For
each connection it saves the data received in a sequence of files, none of which
is larger than the configured size.
'''

from .server import Server
from .config import Config
