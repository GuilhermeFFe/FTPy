# FTPy

A challenge for a job in Cognyte. This software consists in a TCP socket server
that receives raw data and simply stores it in files.

## Rules

- [x] **R1:** The software's port that will receive data must be configurable
      into a configuration file;
- [x] **R2:** The size of the saved files must also be configurable;
- [x] **R3:** Use TCP communication;
- [x] **R4:** All transmitted data must be stored correctly;
- [x] **R5:** Files cannot exceed the maximum under any circumstances;
- [x] **R6:** If there is a need to store data in more than one file, the
      immediately preceding file must have exactly the maximum size configured,
      that is, it cannot be smaller than the defined maximum limit;
- [x] **R7:** The file name must be configurable, as a prefix. At the time the
      file is opened, a timestamp must be concatenated to filename prefix.
- [x] **R8:** The server must activate a configurable timer and, if the client
      does not transmit data for a period equal to or greater than this period,
      the server must cancel the connection;
- [x] **R9:** Allow multiple clients to connect simultaneously;
- [x] **R10:** Ensure that the data for each connection is stored in separate
      files, each with its own sequence.

## Developing time and difficulties

The total developing time needed for this project is approximatedly 6 hours,
divided in 5 days. This includes documentation, testing, understanding the
requirements and implementing and reviewing the code - overall focused time.

The main difficulties faced when developing this project were understanding and
organizing implementation ideas and the organization of automated tests. The
requirements were clear and simple to follow.

## Requirements

To run the software you must use at least python version 3.9 (or in docker, with
the given Dockerfile). Along with python, pip3 must also be installed, to manage
the project's requirements.

## Running with docker

To run with docker, simply build and tag the `Dockerfile` located in the root of
this project, using:

```terminal
$ docker build -t ftpy:latest .
```

This will copy all the files to the container and create the image. After that,
you simply need to take note of the port that the server will listen and the
output directory configured on the `config.json`, to forward the port and mount
the volume needed to see the output files. The following command takes that to
account (if you change the config, you must change the destination port and
directory into account):

```terminal
$ LOCAL_PORT=1234             # the local port that docker will expose
$ LOCAL_DIR=output            # the path to the desired output directory
$ CONTAINER_PORT=1234         # the port that the app will listen inside the container (check config.json)
$ CONTAINER_DIR=/ftpy/output  # the output directory that the app will save files inside the container (check config.json, relative to /ftpy)
$ docker run --name ftpy -d -p $LOCAL_PORT:$CONTAINER_PORT -v $LOCAL_DIR:$CONTAINER_DIR ftpy:latest
```

Now the server is waiting for connections, with the correct ports forwarded!

### Testing in docker

After building the image, testing in docker is made simple by only passing the
`behave` command to docker:

```terminal
$ docker run --rm ftpy:latest behave
```
## Running locally

### 1. Virtual environment (optional, highly recommended)

A virtual environment is a convenient tool to encapsulate and control python
dependencies on a project by project basis. To setup a virtual environment, run:

```terminal
$ pip3 install virtualenv
$ python3 -m virtualenv venv
```

Then, after setting up the environment, it must be activated on every terminal
used:

```terminal
$ source venv/bin/activate
```

### 2. Requirements

* behave: a testing framework, which is used to test if the above mentioned
rules work correctly;
* yapf: a code formatting tool - check `.style.yapf` in the project.

To run the server, you must install the requirements using pip:

```terminal
$ pip3 install -r requirements.txt
```

This will install:

* `behave`: a testing framework, which is used to test if the above mentioned
rules work correctly - the version used for development is 1.2.6, the stable
release at the time of developing;
* `yapf`: a code formatting tool - check `.style.yapf` in the project, using
version 0.31.0, the last stable release at the time of developing.

### 3. Running

The main python file is located in the root of the project, named `main.py`. you
can run it with the `-h` flag:

```terminal
$ python3 main.py -h
usage: main.py [-h] [-c config_file.json]

FTPy: a socket file receiver!

options:
  -h, --help           show this help message and exit
  -c config_file.json  Server json configuration file
```

The configuration file is optional and, if not passed, the software will use the
default values for all possible values. You have the option to also configure
the server partially (only a few values). The possible fields are:

* `"MaxFileSize": int` - maximum size of the generated files. Default is `1024`;
* `"Host": str` - host that the server will listen. Default is `"localhost"`;
* `"Port": int` - port that the server will listen. Default is `6969`;
* `"OutputDir": str` - output directory that the server will save the generated
files. Default is nothing (save directly on project root);
* `"FilePrefix": str` - a prefix that the server will add to generated files.
Default is nothing (no prefix);
* `"Timeout": int` - timeout in seconds to wait after any client has stopped
sending data before disconnecting. Default is `10` seconds.

After this, the FTPy server is running and listening on the given host/port for
TCP connections and data.

### Testing

To run the tests, you must first follow steps 1 and 2 of the `Running` tutorial.
After this, you simply call behave on the project directory, like:

```terminal
$ behave
```